const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../src/Interfaces/HTTP/Server");
const auth = require("../src/Interfaces/HTTP/Middleware/Auth");
//const should = chai.should();
chai.use(chaiHttp);

describe("Users", () => {
  it("Should add a single user on /register post", done => {
    chai
      .request(app)
      .post("/register", auth)
      .send({
        userId: 12345,
        userName: "jr",
        email: "jr@gmail.com",
        password: "123456"
      })
      .end((err, result) => {
        if (err) throw TypeError(err);
        result.should.have.status(200);
        console.log("Body", result.body);
      });
    done();
  });
  it("Should login a user using email and password", done => {
    const login = {
      email: "jr@gmail.com",
      password: "123456"
    };
    chai
      .request(app)
      .post("/login", auth)
      .send(login)
      .end((err, result) => {
        result.should.have.status(200);
        console.log("Body", result.body);
      });
    done();
  });
});

describe("Todo CRUD", () => {
  const todo = {
    todoId: "f1cec450-386a-11ea-9183-5d3f6f2e2a27",
    title: "Todo",
    description: "test todo",
    completed: "true"
  };
  it("Should add a single todo on /create POST", done => {
    chai
      .request(app)
      .post("/create")
      .send(todo)
      .end((err, result) => {
        result.should.have.status(200);
        console.log("Body", result.body);
      });
    done();
  });

  it("Should get a single todo on /find/:todoId ", done => {
    chai
      .request(app)
      .get("/find/" + todo.todoId)
      .send(todo)
      .end((err, result) => {
        result.should.have.status(200);
        console.log("Fetch todo using GET/find:todoId");
      });
    done();
  });

  it("Should update a single todo on /update/:todoId PATCH", done => {
    chai
      .request(app)
      .patch("/update/" + todo.todoId)
      .send(todo)
      .end((err, result) => {
        result.should.have.status(200);
        result.body.data.completed.should.eq(true);
        console.log("updated todo");
      });
    done();
  });

  it("Should delete a single todo on /delete/:todoId DELETE", done => {
    chai
      .request(app)
      .delete("/delete/" + todo.todoId)
      .end((err, result) => {
        result.should.have.status(200);
        console.log("Todo deleted");
      });
    done();
  });
});
