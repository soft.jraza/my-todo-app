const serverConfig = require("./Server.config").server;
const Mongodb = require("./Mongo.config").database.mongo;
const googleConfig = require("./Google.config").google;

module.exports = {
  serverConfig,
  Mongodb,
  googleConfig
};
