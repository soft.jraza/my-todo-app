const env = require("common-env")();
require("dotenv").config();

module.exports = {
  server: {
    NODE_ENV: env.getOrElse("NODE_ENV", "development"),
    APP_NAME: env.getOrElse("APP_NAME", "Todo-App"),
    APP_SECRET: env.getOrElse("APP_SECRET", ""),
    APP_PORT: env.getOrElse("APP_PORT", "5000")
  }
};
