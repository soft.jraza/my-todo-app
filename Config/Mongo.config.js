require("dotenv").config();
const env = require("common-env")();

module.exports = {
  database: {
    mongo: {
      DB_PORT: env.getOrElse("MONGODB_PORT", "27017"),
      DB_HOST: env.getOrElse("MONGODB_HOST", "mongo"),
      DB_NAME: env.getOrElse("MONGODB_NAME", "Todos"),
      DB_USER: env.getOrElse("MONGODB_USER", ""),
      DB_PASS: env.getOrElse("MONGODB_PASS", "")
    }
  }
};
