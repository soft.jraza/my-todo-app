# Todo-App

This project is created with Node.js, MongoDB, Express.js also use jade template for frontend design that can register the user and also login with jwt authentication.

## Available Script

In the project directory, you can run

### `npm run dev`

Run the app in development mode.
Open http://localhost:5000 to view it into the browser.

The page will automatically reload if you make any change.
You will also see the errors in console.

### `npm test`

Launches the test runner in the interactive watch mode.
