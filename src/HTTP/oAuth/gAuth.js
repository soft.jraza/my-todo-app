const googleApi = require("./gapi")

const requestGmailAuth = (req, res, next) => {
  const scopes = [
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/userinfo.profile"
  ]
  try {
    let url = googleApi.generateUrl(scopes)
    return res.redirect(url)
  } catch (e) {
    next(e)
  }
}

const getGmailUserInfo = async (req, res, next) => {
  const { code } = req.query
  try {
    const userData = await googleApi.getUserInfo(code)
    return res.render("register", {
      userInfo: {
        loggedIn: false,
        email: userData.email,
        name: userData.name
      }
    })
  } catch (e) {
    next(e)
  }
}

module.exports = {
  requestGmailAuth,
  getGmailUserInfo
}
