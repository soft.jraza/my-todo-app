const { google } = require("googleapis");
const credentials = require("../../../Config").googleConfig;

class googleApi {
  constructor() {
    const { CLIENT_ID, CLIENT_SECRET, REDIRECT_URIS } = credentials;
    this.oAuth2Client = new google.auth.OAuth2(
      CLIENT_ID,
      CLIENT_SECRET,
      REDIRECT_URIS
    );
  }

  generateUrl(scopes) {
    const url = this.oAuth2Client.generateAuthUrl({
      access_type: "offline",
      scope: scopes.join(" "),
      prompt: "consent"
    });
    return url;
  }

  async getUserInfo(code) {
    const credentials = await this.oAuth2Client.getToken(code);
    this.oAuth2Client.setCredentials(credentials.tokens);
    const gmail = google.oauth2({
      version: "v2",
      auth: this.oAuth2Client
    });
    const userInfo = await gmail.userinfo.get();
    return userInfo.data;
  }
}

module.exports = new googleApi();
