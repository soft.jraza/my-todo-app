const { createLogger } = require("bunyan");

const log = createLogger({
  name: "Todo-App",
  streams: [
    {
      level: "debug",
      stream: process.stdout
    },
    {
      level: "error",
      stream: process.stdout
    }
  ]
});

module.exports = log;
