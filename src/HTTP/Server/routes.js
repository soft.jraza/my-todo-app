const app = require("./bootstrap")
const appRoute = require("../Routes/AppRoute")

app.use("/", appRoute)

module.exports = app
