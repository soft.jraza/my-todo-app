const express = require("express");
const session = require("express-session");
const path = require("path");
const Middleware = require("../Middleware/middleware");
const app = express();

Middleware(app);

app.set("view engine", "pug");
app.set("views", path.join(__dirname, "../../../views"));
app.use(express.static(path.join(__dirname, "../../../public")));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(
  session({
    secret: "123itsSecRet456!",
    resave: true,
    saveUninitialized: true
  })
);

module.exports = app;
