const express = require("express");
const auth = require("../Middleware/Auth");
const user = require("./UserRoute");
const todo = require("./TodoRoute");
const google = require("../oAuth/gAuth");

const router = express.Router();

// User App Routes
router.get("/auth/google", google.requestGmailAuth);
router.get("/auth/google/callback", google.getGmailUserInfo);
router.get("/register", user.registerPage);
router.post("/register", user.registerUser);
router.get("/login", user.loginPage);
router.post("/login", user.loginUser);
router.get("/profile", auth, user.profilePage);
router.get("/about", user.aboutpage);
router.get("/logout", user.logOut);
router.get("/", user.homePage);

//Todo Api routes
router.post("/create", todo.createTodo);
router.get("/find/:todoId", todo.fetchTodoById);
router.patch("/update/:todoId", todo.updateTodo);
router.delete("/delete/:todoId", todo.removeTodo);

module.exports = router;
