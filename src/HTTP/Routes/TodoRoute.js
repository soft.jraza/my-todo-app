const { CommandBus, LoggerMiddleware } = require("simple-command-bus");
const commandHandlerMiddelware = require("../../Command/Handlers");
const logger = require("../logger/logger");
const CreateTodoCommand = require("../../Command/TodoCommand/CreateTodoCommand");
const FetchByIdCommand = require("../../Command/TodoCommand/FetchByIdCommand");
const RemoveCommand = require("../../Command/TodoCommand/RemoveCommand");
const UpdateCommand = require("../../Command/TodoCommand/UpdateCommand");

const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddelware
]);

/**
 * Create new todo with all details
 * @param {String} req
 * @param {String} res
 */
const createTodo = async (req, res) => {
  const { title, description, completed } = req.body;
  try {
    const createTodoCommand = new CreateTodoCommand(
      title,
      description,
      completed
    );
    const todoResult = await commandBus.handle(createTodoCommand);
    return res.send(todoResult);
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Get todo by todoId and userId
 * @param {String} req
 * @param {String} res
 */
const fetchTodoById = async (req, res) => {
  const { todoId } = req.params;
  try {
    const fetchByIdCommand = new FetchByIdCommand(todoId);
    const todoResult = await commandBus.handle(fetchByIdCommand);
    return res.send(todoResult);
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Update todo with validation
 * @param {String} req
 * @param {String} res
 */
const updateTodo = async (req, res) => {
  try {
    const { todoId } = req.params;
    const { title, description, completed } = req.body;

    const updateCommand = new UpdateCommand(
      todoId,
      title,
      description,
      completed
    );
    await commandBus.handle(updateCommand);
    return res.send({ message: "Updated successfully" });
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Remove todo with condition
 * @param {String} req
 * @param {String} res
 */
const removeTodo = async (req, res) => {
  try {
    const { todoId } = req.params;
    const removeCommand = new RemoveCommand(todoId);
    await commandBus.handle(removeCommand);
    return res.send({ message: "Successfully removed." });
  } catch (e) {
    logger.debug(e);
  }
};

module.exports = {
  createTodo,
  fetchTodoById,
  updateTodo,
  removeTodo
};
