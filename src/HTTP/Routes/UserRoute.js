const { CommandBus, LoggerMiddleware } = require("simple-command-bus");
const commandHandlerMiddleware = require("../../Command/Handlers");
const CreateUserCommand = require("../../Command/UserCommand/CreateUserCommand");
const UserLoginCommand = require("../../Command/UserCommand/UserLoginCommand");

const logger = require("../logger/logger");
const jwt = require("jsonwebtoken");
const config = require("../../../Config").serverConfig;

const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
]);

/**
 * Create new user with all details
 * @param {*} req
 * @param {*} res
 */
const registerUser = async (req, res) => {
  const { userName, email, password } = req.body;
  try {
    const createUserCommand = new CreateUserCommand(userName, email, password);
    const user = await commandBus.handle(createUserCommand);
    if (user) {
      req.session.user = {
        name: user.userName,
        email: user.email,
        loggedIn: true
      };
    }
    const token = jwt.sign({ userId: user.userId }, config.APP_SECRET);
    req.session.token = token;
    return res.redirect("/profile");
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Login user by credentials
 * @param {body} req
 * @param {*} res
 */
const loginUser = async (req, res) => {
  const { email, password } = req.body;
  try {
    const userLoginCommand = new UserLoginCommand(email, password);
    const user = await commandBus.handle(userLoginCommand);
    if (user) {
      req.session.user = {
        email: user.email,
        name: user.userName,
        loggedIn: true
      };
    }
    const token = jwt.sign({ userId: user.userId }, config.APP_SECRET);
    req.session.token = token;
    return res.redirect("/profile");
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Render the Home page
 * @param {*} req
 * @param {*} res
 */
const homePage = (req, res) => {
  try {
    let locals = {
      title: "HomePage",
      userInfo: { loggedIn: false }
    };
    return res.render("index", locals);
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Render the Sign up page if user not loggedIn
 * @param {*} req
 * @param {*} res
 */
const registerPage = (req, res) => {
  try {
    let locals = {
      title: "Sign up",
      userInfo: { loggedIn: false }
    };
    return res.render("register", locals);
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Render the Login page if user not loggedIn
 * @param {*} req
 * @param {*} res
 */
const loginPage = (req, res) => {
  try {
    let locals = {
      title: "Sign in",
      userInfo: { loggedIn: false }
    };
    return res.render("login", locals);
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Render about if user loggedIn, else redirect login page
 * @param {*} req
 * @param {*} res
 */
const aboutpage = (req, res) => {
  try {
    const currentUser = req.session.user;
    let locals = {
      title: "About",
      userInfo: currentUser,
      loggedIn: req.session.loggedIn
    };
    if (currentUser) {
      return res.render("about", locals);
    }
    return res.redirect("/login");
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Render profile if uer loggedIn, else redirect login page
 * @param {*} req
 * @param {*} res
 */
const profilePage = (req, res) => {
  try {
    const currentUser = req.session.user;
    let locals = {
      userInfo: currentUser,
      loggedIn: req.session.loggedIn
    };
    if (currentUser) {
      return res.render("profile", locals);
    }
    return res.redirect("/login");
  } catch (e) {
    logger.debug(e);
  }
};

/**
 * Clear session detail and redirect to login page
 * @param {*} req
 * @param {*} res
 */
const logOut = (req, res) => {
  try {
    req.session.user = { email: "", name: "", loggedIn: false };
    return res.redirect("/login");
  } catch (e) {
    logger.debug(e);
  }
};
module.exports = {
  registerUser,
  loginUser,
  homePage,
  registerPage,
  loginPage,
  aboutpage,
  profilePage,
  logOut
};
