const commonMiddleware = require("./common");

const Middleware = app => {
  commonMiddleware(app);
};

module.exports = Middleware;
