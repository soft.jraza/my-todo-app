const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const morgan = require("morgan");
const cors = require("cors");
const helmet = require("helmet");

const commonMiddleware = app => {
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(morgan("common"));
  app.use(cors());
  app.use(helmet());
};

module.exports = commonMiddleware;
