const jwt = require("jsonwebtoken");
const config = require("../../../Config").serverConfig;
const User = require("../../Infrastructure/Models/UserModel");

const authorization = async (req, res, next) => {
  try {
    const token = req.session.token;
    if (!token)
      return res.status(401).json({ message: "Authentication error" });
    const decoded = jwt.verify(token, config.APP_SECRET);
    const user = User.findOne({
      userId: decoded.userId
    });
    if (!user) throw new TypeError();

    req.user = user;
    next();
  } catch (e) {
    console.log(e);
    res.status(500).send({ message: "Invalid Token" });
  }
};

module.exports = authorization;
