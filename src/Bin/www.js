const program = require("commander");
const logger = require("../HTTP/logger/logger");
const app = require("../HTTP/Server");
const config = require("../../Config").serverConfig;

require("dotenv").config();
require("../Infrastructure/Connections/mongo");

program.action(() => {
  app.listen(config.APP_PORT, () => {
    logger.debug(`${config.APP_NAME} Listening on port ${config.APP_PORT}`);
  });
});

program.parse(process.argv);
