const uuid = require("uuid/v1");

class Todo {
  constructor(todoId, title, description, completed) {
    this.todoId = todoId;
    this.title = title;
    this.description = description;
    this.completed = completed;
  }

  /**
   * Create Todo with details
   * @param {String} title
   * @param {String} description
   * @param {String} completed
   */
  static createFromDetails(title, description, completed) {
    return new Todo(uuid(), title, description, completed);
  }

  /**
   * Create Todo with object
   * @param {Object} obj
   */
  static createFromObject(obj) {
    return new Todo(obj.todoId, obj.title, obj.description, obj.completed);
  }
}

module.exports = Todo;
