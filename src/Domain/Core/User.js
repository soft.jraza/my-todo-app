const uuid = require("uuid/v1");

class User {
  constructor(userId, userName, email) {
    this.userId = userId;
    this.userName = userName;
    this.email = email;
    this.password = "";
  }

  /**
   * Store the password
   * @param {String} password
   */

  setPassword(password) {
    this.password = password;
  }

  /**
   * Create User with details
   * @param {String} userName
   * @param {String} email
   */
  static createFromDetails(userName, email) {
    return new User(uuid(), userName, email);
  }

  /**
   * Create User with object
   * @param {Object} obj
   */
  static createFromObject(obj) {
    return new User(obj.userId, obj.userName, obj.email);
  }
}

module.exports = User;
