const userRepository = require("../../Infrastructure/MongoRepository/UserRepository");
const logger = require("../../HTTP/logger/logger");
const bcrypt = require("bcrypt");
const User = require("../Core/User");

class UserService {
  /**
   * Create new user and check validation
   * @param {String} userName
   * @param {String} email
   * @param {String} password
   * @returns {Object}
   */

  static async newUser(userObj) {
    try {
      const { userName, email, password } = userObj;
      const user = User.createFromDetails(userName, email);
      let checkEmail = await userRepository.checkEmail(user.email);
      if (checkEmail) throw new Error("Email is already registered.");
      user.setPassword(bcrypt.hashSync(password, 9));
      return await userRepository.add(user);
    } catch (e) {
      logger.debug(e);
    }
  }

  /**
   * Get user by email from database
   * @param {String} Email
   * @param {String} password
   * @returns {Object}
   */

  static async getByCredentials(email, password) {
    try {
      const userLogin = await userRepository.fetchByCredentials(email);
      if (!userLogin) throw new Error("Invalid Email.");
      const isMatch = await bcrypt.compare(password, userLogin.password);
      if (!isMatch) throw new Error("Password does not match.");
      return userLogin;
    } catch (e) {
      logger.debug(e);
    }
  }
}

module.exports = UserService;
