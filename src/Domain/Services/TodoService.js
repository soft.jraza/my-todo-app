const todoRepository = require("../../Infrastructure/MongoRepository/TodoRepository"),
  logger = require("../../HTTP/logger/logger"),
  Todo = require("../Core/Todo");

class TodoService {
  /**
   * Store new todo in database
   * @param {Object} todoObj
   */

  static async newTodo(todoObj) {
    try {
      const { title, description, completed } = todoObj;
      const addTodo = Todo.createFromDetails(title, description, completed);
      return await todoRepository.create(addTodo);
    } catch (e) {
      logger.debug(e);
    }
  }

  /**
   * Get todo by uuid() from database
   * @param {unique} todoId
   * @returns {Object}
   */

  static async getById(todoId) {
    try {
      const todoObj = await todoRepository.fetchById(todoId);
      return Todo.createFromObject(todoObj);
    } catch (e) {
      logger.debug(e);
    }
  }

  /**
   * Update todo by uuid() from database
   * @param {unique} todoId
   * @param {String} todoUpdate
   * @returns {Object}
   */

  static async update(todoId, updateTodo) {
    try {
      const todoObj = await todoRepository.update(todoId, updateTodo);
      return Todo.createFromObject(todoObj);
    } catch (e) {
      logger.debug(e);
    }
  }

  /**
   * Delete todo by uuid() in database
   * @param {unique} todoId
   */

  static async delete(todoId) {
    try {
      return await todoRepository.delete(todoId);
    } catch (e) {
      logger.debug(e);
    }
  }
}

module.exports = TodoService;
