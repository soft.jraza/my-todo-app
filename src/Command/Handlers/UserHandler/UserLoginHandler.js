const userService = require("../../../Domain/Services/UserService")

class UserLoginHandler {
  async handle(command) {
    const user = await userService.getByCredentials(
      command.email,
      command.password
    )
    return user
  }
}

module.exports = UserLoginHandler
