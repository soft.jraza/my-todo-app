const userService = require("../../../Domain/Services/UserService")

class CreateUserHandler {
  async handle(command) {
    const user = await userService.newUser(command)
    return user
  }
}

module.exports = CreateUserHandler
