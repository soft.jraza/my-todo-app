const {
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector
} = require("simple-command-bus")

const CreateUserHandler = require("./UserHandler/CreateUserHandler")
const UserLoginHandler = require("./UserHandler/UserLoginHandler")
const CreateTodoHandler = require("./TodoHandler/CreateTodoHandler")
const FetchByIdHandler = require("./TodoHandler/FetchByIdHandler")
const UpdateHandler = require("./TodoHandler/UpdateTodoHandler")
const RemoveHandler = require("./TodoHandler/RemoveTodoHandler")

const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({
    CreateUserHandler: new CreateUserHandler(),
    UserLoginHandler: new UserLoginHandler(),
    CreateTodoHandler: new CreateTodoHandler(),
    FetchByIdHandler: new FetchByIdHandler(),
    UpdateHandler: new UpdateHandler(),
    RemoveHandler: new RemoveHandler()
  }),
  new HandleInflector()
)

module.exports = commandHandlerMiddleware
