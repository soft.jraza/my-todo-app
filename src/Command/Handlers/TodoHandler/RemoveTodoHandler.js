const todoService = require("../../../Domain/Services/TodoService")

class RemoveTodoHandler {
  async handle(command) {
    return await todoService.delete(command.todoId)
  }
}

module.exports = RemoveTodoHandler
