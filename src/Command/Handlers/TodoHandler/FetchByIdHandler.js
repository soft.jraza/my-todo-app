const todoService = require("../../../Domain/Services/TodoService")

class FetchByIdHandler {
  async handle(command) {
    return await todoService.getById(command.todoId)
  }
}

module.exports = FetchByIdHandler
