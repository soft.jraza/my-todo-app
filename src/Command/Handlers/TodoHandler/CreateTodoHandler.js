const todoService = require("../../../Domain/Services/TodoService")

class CreateTodoHandler {
  async handle(command) {
    return await todoService.newTodo(command)
  }
}

module.exports = CreateTodoHandler
