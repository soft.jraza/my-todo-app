const { Command } = require("simple-command-bus");

class CreateTodoCommand extends Command {
  /**
   * Create Todo details
   * @param {String} title
   * @param {String} description
   * @param {String} completed
   */
  constructor(title, description, completed) {
    super();
    this.title = title;
    this.description = description;
    this.completed = completed;
  }
}

module.exports = CreateTodoCommand;
