const { Command } = require("simple-command-bus");

class RemoveCommand extends Command {
  /**
   * Remove by todoId
   * @param {unique} todoId
   */
  constructor(todoId) {
    super();
    this.todoId = todoId;
  }
}

module.exports = RemoveCommand;
