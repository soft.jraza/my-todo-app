const { Command } = require("simple-command-bus");

class FetchByIdCommand extends Command {
  /**
   * Get by todoId
   * @param {unique} todoId
   */
  constructor(todoId) {
    super();
    this.todoId = todoId;
  }
}

module.exports = FetchByIdCommand;
