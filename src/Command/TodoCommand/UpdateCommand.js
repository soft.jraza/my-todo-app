const { Command } = require("simple-command-bus");

class UpdateCommand extends Command {
  /**
   * Update by req.body
   * @param {unique} todoId
   * @param {String} title
   * @param {String} description
   * @param {String} completed
   */
  constructor(todoId, title, description, completed) {
    super();
    this.todoId = todoId;
    this.title = title;
    this.description = description;
    this.completed = completed;
  }
}

module.exports = UpdateCommand;
