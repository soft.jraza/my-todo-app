const { Command } = require("simple-command-bus");

class CreateUserCommand extends Command {
  /**
   * Create Client details
   * @param {String} userName
   * @param {String} email
   * @param {String} password
   */
  constructor(userName, email, password) {
    super();
    this.userName = userName;
    this.email = email;
    this.password = password;
  }
}

module.exports = CreateUserCommand;
