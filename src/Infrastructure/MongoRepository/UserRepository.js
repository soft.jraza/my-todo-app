const userModel = require("../Models/UserModel");

class userRepository {
  /**
   *  Create new user in database
   * @param {Object} user
   */

  static async add(user) {
    return await userModel.create(user);
  }

  /**
   * Find User by Email from database
   * @param {String} email
   */
  static async fetchByCredentials(email) {
    return await userModel.findOne({ email });
  }

  /**
   * Fetch user email for validation
   * @param {String} email
   */
  static async checkEmail(email) {
    return userModel.findOne({ email });
  }
}

module.exports = userRepository;
