const todoModel = require("../Models/TodoModel");
const Todo = require("../../Domain/Core/Todo");

class TodoRepository {
  /**
   * createTodo() Store todo in database
   * @param {Object} todo
   */

  static async create(todo) {
    return await todoModel.create(todo);
  }

  /**
   * Fetch todo by uuid() from databse
   * @param {unique} todoId
   */

  static async fetchById(todoId) {
    return await todoModel.findOne({ todoId });
  }

  /**
   * Update todo by uuid() in database
   * @param {unique} todoId
   * @param {String} todoUpdate
   */

  static async update(todoId, todoUpdate) {
    return await todoModel.findOneAndUpdate({ todoId }, todoUpdate);
  }

  /**
   * Delete todo by uuid() in database
   * @param {unique} todoId
   */

  static async delete(todoId) {
    return todoModel.findOneAndRemove({ todoId });
  }
}
module.exports = TodoRepository;
