const mongoose = require("mongoose")

const UserSchema = new mongoose.Schema(
  {
    userId: {
      type: String
    },
    userName: {
      type: String
    },
    email: {
      type: String
    },
    password: {
      type: String
    }
  },
  { timestamps: true }
)

const userModel = mongoose.model("user", UserSchema)
module.exports = userModel
