const mongoose = require("mongoose");

const todoSchema = new mongoose.Schema(
  {
    todoId: {
      type: String
    },
    title: {
      type: String
    },
    description: {
      type: String
    },
    completed: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);
const todo = mongoose.model("todo", todoSchema);
module.exports = todo;
