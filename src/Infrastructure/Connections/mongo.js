const mongoose = require("mongoose");
const logger = require("../../HTTP/logger/logger");
const config = require("../../../Config").Mongodb;

mongoose.Promise = global.Promise;

mongoose
  .connect(`mongodb://${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  })
  .then(() => console.log("Connected to MongoDB..."))
  .catch(() => console.error("Could not connects to MongoDB..."));

logger.debug(`mongodb://${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`);
